<?php

namespace Drupal\Tests\commerce_webform_order\Functional\WebformHandler;

use Drupal\Tests\commerce_webform_order\Functional\CommerceWebformOrderTestBase;

/**
 * Tests Commerce Webform Order handler: Hide add to cart message.
 *
 * @group commerce_webform_order
 */
class CommerceWebformOrderHandlerOrderItemTitle extends CommerceWebformOrderTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_webform_order_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $testWebforms = [
    'cwo_test_order_item_title',
  ];

  /**
   * Use a custom order item title.
   *
   * In this test we are going to check the order item title:
   *   - After submit, the order item title should be
   *     'Lorem ipsum dolor sit amet'.
   */
  public function testOrderItemTitle() {
    // Test as anonymous user.
    $this->drupalLogout();

    $webform_submission = $this->postProductVariationToWebform('ONE', 'cwo_test_order_item_title');
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $this->orderItemRepository->getLastByWebformSubmission($webform_submission, 'commerce_webform_order_handler');

    // The order item ID is stored in the configured element.
    $this->assertEquals('Lorem ipsum dolor sit amet', $order_item->label());
  }

}
