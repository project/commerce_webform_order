<?php

namespace Drupal\Tests\commerce_webform_order\Kernel\Plugin\WebformHandler;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_webform_order\Plugin\WebformHandler\CommerceWebformOrderHandler;

/**
 * Tests the CommerceWebformOrderHandler webform handler.
 *
 * @group commerce_webform_order
 */
class CommerceWebformOrderHandlerTest extends CommerceKernelTestBase {

  use StringTranslationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_reference_revisions',
    'commerce_cart',
    'commerce_product',
    'commerce_order',
    'commerce_webform_order',
    'path',
    'profile',
    'state_machine',
    'webform',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product');
    $this->installConfig(['commerce_product']);
  }

  /**
   * Tests loading variations by ID and UUID.
   */
  public function testLoadByIdAndUuid() {
    $variation_1 = ProductVariation::create([
      'type' => 'default',
      'variation_id' => '214',
      'uuid' => 'd53c146e-e420-42b5-bef5-d344682acd96',
      'sku' => $this->randomMachineName(),
      'title' => $this->randomString(),
    ]);
    $variation_1->save();

    $variation_2 = ProductVariation::create([
      'type' => 'default',
      'variation_id' => '1080',
      'uuid' => '214abdb0-cc1e-4f67-bbb9-3e6e2a604bdf',
      'sku' => $this->randomMachineName(),
      'title' => $this->randomString(),
    ]);
    $variation_2->save();

    /** @var \Drupal\webform\Plugin\WebformHandlerManagerInterface $handler_manager */
    $handler_manager = $this->container->get('plugin.manager.webform.handler');

    $reflection = new \ReflectionClass(CommerceWebformOrderHandler::class);
    $method = $reflection->getMethod('loadEntityValue');
    $cwo_handler = $handler_manager->createInstance('commerce_webform_order', []);
    $variation_3 = $method->invoke(
      $cwo_handler,
      '214abdb0-cc1e-4f67-bbb9-3e6e2a604bdf',
      'commerce_product_variation',
      ['uuid', 'variation_id', 'sku'],
      TRUE
    );

    $this->assertEquals($variation_2->uuid(), $variation_3->uuid());
  }

}
